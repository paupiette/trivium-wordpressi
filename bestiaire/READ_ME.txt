#super dessins sur le doc ! 

//////////////////////////////////////////////////
#pluggin à avoir:
- ewww-image-optimizer
- ACF

#navigateur à utiliser : chrome

1.installez votre thème sur un wordpress en local host:
- copier le dossier "bestiaire/" dans "wordpress/wp-content/themes/"

2.Activez votre thème :
- Rendez-vous dans votre espace administrateur sur votre wordpress,
- Cliquez sur l'onglet apparence à gauche,
- Repérez le thème bestiaire et cliquez sur activer.

3.Pour une utilisation optimale du thème: 
- Cliquez sur l'onglet Page (à gauche) et créer 2 pages : l'une servira d'accueil statique et l'autre sera une page dynamique qui contiendra les articles créés. 

- Cliquez sur l'onglet "Réglage" et le sous-onglet "Lecture",
- À "La page d'accueil affiche" sélectionnez "Une page statique (choisir ci-dessous)" puis choisissez les pages que vous avez créer précédemment.

- Pour finir, retournez dans l'onglet "Apparence"(à gauche) et le sous-onglet "Arrière-plan" et choisissez une image de fond que vous souhaitez afficher sur la page d'accueil statique. 

4.Pour la création d'un article:
- Entrez un titre,
- Créez  un paragraphe (ce sera le contenu principal de votre article),
- Ensuite utilisez le formulaire "Animal" pour ajouter une image, donner l'origine de votre animal et faire des liens vers d'autres articles ou pages (utilisé pour définir l'alimentation de votre animal). 

  

