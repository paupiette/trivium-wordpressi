<?php
if( ! function_exists( 'bestiaire_scripts' ) ):
/**
 * Si la fonction n'existe pas : pour éviter les gros bordel entre theme enfant et theme parent
 * chargement de ma feuille de style : style.css
**/
function bestiaire_scripts() {
    wp_enqueue_style( 'bestiaire-styles', get_stylesheet_uri(), array(), null );
}
endif;

/**
 * charge les scripts et styles contenu dans la fonction 'bestiaire_scripts'
 */
add_action( 'wp_enqueue_scripts', 'bestiaire_scripts');

//Pour la gestion des menus
add_theme_support('menus');
//registre des menu et leur position'
register_nav_menu('header', 'menu_principal');

//Pour la gestion des images de fond personalisable
$defaults = array (
    'default-color' => '',
    'default-image' => '',
    'default-repeat' => '',
    'default-position-x' => '',
    'default-attachment' => '',
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
);

add_theme_support ('custom-background', $defaults);


if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_6027aeb65491e',
        'title' => 'Animal',
        'fields' => array(
            array(
                'key' => 'field_6027af75f4b9e',
                'label' => 'image animal',
                'name' => 'image_animal',
                'type' => 'image',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => 'img_animal',
                    'id' => '',
                ),
                'return_format' => 'url',
                'preview_size' => 'medium',
                'library' => 'all',
                'min_width' => '',
                'min_height' => '',
                'min_size' => '',
                'max_width' => '',
                'max_height' => '',
                'max_size' => '',
                'mime_types' => '.jpg',
            ),
            array(
                'key' => 'field_6027b06498fa7',
                'label' => 'origine',
                'name' => 'origine_animal',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => 'Originaire de la russie',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_6027b12c98fa8',
                'label' => 'alimentation',
                'name' => 'alimentation_animal',
                'type' => 'relationship',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => '',
                'taxonomy' => '',
                'filters' => array(
                    0 => 'search',
                    1 => 'post_type',
                    2 => 'taxonomy',
                ),
                'elements' => '',
                'min' => '',
                'max' => '',
                'return_format' => 'object',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;