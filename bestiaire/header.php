<?php
/*
Theme Name: Bestiaire
*/
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <!--[if IE]>
	<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=edge" /><![endif]-->
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>bestiaire</title>
    
    <?php wp_head(); ?>
    <!--lié à functions.php, contient la fonction 'bestiaire_script':
    qui permet de faire le lien avec style.css / on écrit pas le chemain en dur dans les thèmes 
    wordpress-->

</head>
<body class="custom-background bestiaire-body">
    <header id="header" class="bestiaire-header">
        <!--le menu et sa position-->
        <nav class="bestiaire_nav">
        <?php 
        wp_nav_menu([
        'theme_location' => 'header',
        'menu_class' => 'bestiaire_ul',//met une classe sur les <ul>
        
        ]);
        ?>
        </nav>
    </header>
    
       
       
        