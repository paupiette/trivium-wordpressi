<?php get_header()?>
<!--méthode de WP permettant de faire une sorte d'include de header.php-->

<div class="div-bestiaire-home1">
    <h1 class="title-bestiaire-home"> <?php wp_title(' ');?> </h1>

    <div class="div-bestiaire-home2">
        <main id="content" class="content main-bestiaire-home">
        
        <!--boucle d'affichage des articles
        query_posts('orderby=post_title & order=asc') -> permet d'afficher par ordre alphabétique-->
        <?php if ( have_posts() ) : query_posts('orderby=post_title & order=asc'); while ( have_posts() ) : the_post();?> 
    
            <div class="div-content-bestiaire-home">
                <h2 class="title2-bestiaire-home"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
                <div class="content-bestiaire-home"><?php the_content(); ?><div>
            </div>
        
        <?php endwhile; else: ?>
        <p><?php _e('Pas d\' article trouvé'); ?></p>
        <?php endif; ?>
        <!--fin de la boucle-->

        </main>
    </div>
</div>
    
    
<?php get_footer()?><!--permet d'afficher un footer.php (celui de base dans WP ou un créer dans notre template)-->

