<?php get_header()?>
<!--méthode de WP permettant de faire une sorte d'include de header.php
WP oblige à travailler avec des nom de fichiers bien précis (voir la doc) afin de 'coller' avec
ses propres fichiers = son coeur-->

<div class="bestiaire-div-index">
    <main class="main-bestiaire-index">
        <h1 class="title-bestiaire-index"> <?php the_title();?></h1>
    </main>
</div>

<?php get_footer()?><!--permet d'afficher un footer.php (celui de base dans WP ou un créer dans notre template)-->

<!--the_field('image_garde')-->