<?php get_header()?>
<!--méthode de WP permettant de faire une sorte d'include de header.php
WP oblige à travailler avec des nom de fichiers bien précis (voir la doc) afin de 'coller' avec
ses propres fichiers = son coeur-->
<div class="div-bestiaire-single1">
    <div class="div-bestiaire-single2">
        <h1 class="title-single-bestiaire"> <?php the_title();?></h1>

        <main class="main-bestiaire-single">
        <?php
        // Start the loop.
        while ( have_posts() ) : the_post();?>

        <p class="p-bestiaire-single"><?php the_field('origine_animal');?></p>
  
        <div class="div-bestiaire-single3"><?php the_content(); ?></div>

        </main>
    </div>

    <aside class="aside-bestiaire-single"> 
        <img class="img-bestiaire-single" src="<?php the_field('image_animal'); ?>">
        <p class="p-bestiaire-aside-single">Alimentation :</p>

        <!--affichage des relations entre article-->
        <?php $array_bestiaire = get_field('alimentation_animal');
        ?>
        <ul class="ul-bestiaire-single">
        <?php foreach($array_bestiaire as $bete){?> 
            <li><a class="a-bestiaire-single" href="<?php echo $bete -> guid;?>"><?php echo $bete -> post_title; }?></a></li>
        </ul>
        
    </aside>
    <!--<div><?php
     
    /*$url=get_option('blog_url');//récupe dans la bdd
    
    $vuevue=file_get_contents($objet['post'][0]);
    $objetobjet= json_decode($vuevue, true);
    var_dump($objetobjet);*/
    ?></div>-->
<div>
    
    <?php
    // End the loop.
    endwhile;?>

<?php get_footer()?><!--permet d'afficher un footer.php (celui de base dans WP ou un créer dans notre template)-->